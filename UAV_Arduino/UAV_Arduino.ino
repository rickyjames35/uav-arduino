#define MPU9150_Enabled //9 DOF https://www.sparkfun.com/products/11486
#define BMP085_Enabled //Barometric Pressure Sensor https://www.sparkfun.com/products/retired/11282
#define LS2003_Enabled //GPS https://www.sparkfun.com/products/8975
#define MAX_SONAR_Enabled //Ultrasonic Range Finder https://www.sparkfun.com/products/9494
#define AUTTOPILOT_VOLTAGE_Enabled //AttoPilot Voltage and Current Sense Breakout - 90A https://www.sparkfun.com/products/9028
#define REAL_HARDWARE

#include <Servo.h>
#include <MemoryStructs.h>
#include <UAV_VMS.h>
#include <BMP085.h>
#include <Wire.h>
#include "I2Cdev.h"
#include "MPU9150Lib.h"
#include "CalLib.h"
#include <inv_mpu.h>
#include <EEPROM.h>
#include <TinyGPS.h>
#include <UAV_ArduinoSerial.h>
#include <MemoryFree.h>
#include <Autopilot.h>
#include <CoordinateSystemHelper.h>
#include <UAV_INS.h>

//pins
#define PIN_ELEVATOR 2
#define PIN_RUDDER 3
#define PIN_AILERON_LEFT 4
#define PIN_AILERON_RIGHT 5
#define PIN_FLAP_LEFT 6
#define PIN_FLAP_RIGHT 7
#define PIN_THROTTLE 8
#define PIN_SERVO_RELAY 9
#define PIN_SERVO_STATUS 10
#define INT_MAXSONAR 5 //pin 18 on Mega2560
#define PIN_MAXSONAR 18
#define PIN_TEST_LIGHT 13
#define PIN_VOLTAGE A0
#define PIN_CURRENT A1

//settings				
#define SERIAL_BAUDRATE 230400
#define SERIAL2_BAUDRATE 57600
#define MAG_VAR -12.0f
#define YPR_SAMPLE_COUNT 10
#define CONTROL_SURFACE_TIMEOUT 2500
#define RALT_OFFSET -6.0f

//rates (milliseconds)
#define YAW_PITCH_ROLL_SEND_RATE 66
#define HERTZ_SEND_RATE 1000
#define GPS_SEND_RATE 66
#define PRESSURE_TEMP_ALT_RATE 200
#define RALT_SEND_RATE 200
#define VOLTAGE_CURRENT_SEND_RATE 200
#define HIL_INPUT_RATE 66
#define AUTOPILOT_MODE_SEND_RATE 200
#define CURRENT_WAYPOINT_SEND_RATE 200

Autopilot autopilot;
UAV_VMS vms;
UAV_ArduinoSerial uavSerial;
UAV_INS ins;

unsigned long frameCount_;
unsigned long lastHertzSend_;
unsigned long lastYawPitchRollSend_;
unsigned long lastControlSurfaceReceive_;
unsigned long lastGyroAlign_;
unsigned long lastGpsSend_;
unsigned long lastPressureTempAltSend_;
unsigned long lastRaltSend_;
unsigned long lastVoltageCurrentSend_;
unsigned long lastHilInputSend_;
unsigned long lastAutopilotModeSend_;
unsigned long lastCurrentWaypointSend_;
volatile unsigned long interruptRangeTimerStart;
volatile int rangePulseTime;
YawPitchRollStruct yprSamples_[YPR_SAMPLE_COUNT];
PressureTempAltStruct pressureTempAlt;
VoltageCurrentStruct voltageCurrent;
boolean hilEnabled;
boolean servosSet_;

#if defined(BMP085_Enabled)
BMP085 BMP085_;
#endif

#if defined(MPU9150_Enabled)
MPU9150Lib MPU9150_;
#endif

#if defined(LS2003_Enabled)
TinyGPS LS2003_;
#endif

#if defined(REAL_HARDWARE)
//Servos
Servo servoElevator;
Servo servoRudder;
Servo servoAileronLeft;
Servo servoAileronRight;
Servo servoFlapLeft;
Servo servoFlapRight;
Servo servoThrottle;
#endif

void setup()
{
	Serial.begin(SERIAL_BAUDRATE);	
	
	#if defined(LS2003_Enabled)
	Serial2.begin(SERIAL2_BAUDRATE);
	#endif
	
	#if defined(MAX_SONAR_Enabled)
	attachInterrupt(INT_MAXSONAR, calcRange, CHANGE);
	#endif
	
	Serial.setTimeout(10);
	Wire.begin();
	pinMode(PIN_TEST_LIGHT, OUTPUT);
	pinMode(PIN_SERVO_RELAY, OUTPUT);
	pinMode(PIN_SERVO_STATUS, OUTPUT);
	digitalWrite(PIN_SERVO_RELAY, LOW);
	frameCount_ = 0;
	hilEnabled = false;
	servosSet_ = false;

	#if defined(MPU9150_Enabled)
	MPU9150_.selectDevice(0);
	MPU9150_.useMagCal(false);
	MPU9150_.init(20, 10, 10, 40);
	#endif

	#if defined(BMP085_Enabled)
	BMP085_.bmp085Calibration();
	#endif

	autopilot.SetVMS(&vms);
	autopilot.SetSerial(&uavSerial);
	autopilot.SetINS(&ins);
	
	lastHilInputSend_ = 0;
	lastHertzSend_ = 0;
	lastYawPitchRollSend_ = 0;
	lastGpsSend_ = 0;
	lastPressureTempAltSend_ = 0;
	lastRaltSend_ = 0;
	lastVoltageCurrentSend_ = 0;
	interruptRangeTimerStart = 0;
	lastAutopilotModeSend_ = 0;
	lastCurrentWaypointSend_ = 0;
	lastControlSurfaceReceive_ = -CONTROL_SURFACE_TIMEOUT;
}

void loop()
{
	if (!uavSerial.Update())
	{
		asm volatile ("  jmp 0");	
	}		
	
	ins.Update();
	
	unsigned long currentTime = millis();
	
	//if the severs have not been turned on
	if (!servosSet_)
	{
		servosSet_ = true;
		#if defined(REAL_HARDWARE)
		servoElevator.attach(PIN_ELEVATOR);
		servoRudder.attach(PIN_RUDDER);
		servoAileronLeft.attach(PIN_AILERON_LEFT);
		servoAileronRight.attach(PIN_AILERON_RIGHT);
		servoFlapLeft.attach(PIN_FLAP_LEFT);
		servoFlapRight.attach(PIN_FLAP_RIGHT);
		servoThrottle.attach(PIN_THROTTLE);
		#endif
		digitalWrite(PIN_SERVO_RELAY, LOW);
		digitalWrite(PIN_SERVO_STATUS, LOW);
	}
	//if servers set and we have not timed out / lost connection with the ground
	else if (servosSet_ && ((currentTime - lastControlSurfaceReceive_ < CONTROL_SURFACE_TIMEOUT) || autopilot.autopilotActive))
	{
		digitalWrite(PIN_SERVO_RELAY, HIGH);
		digitalWrite(PIN_SERVO_STATUS, HIGH);
		#if defined(REAL_HARDWARE)
		servoElevator.write(vms.GetElevatorOutput());
		servoRudder.write(vms.GetRudderOutput());
		servoAileronLeft.write(vms.GetAileronOutput());
		servoAileronRight.write(vms.GetAileronOutput());
		servoFlapLeft.write(180 - vms.GetFlapsOutput());
		servoFlapRight.write(vms.GetFlapsOutput());		
		
		if (!hilEnabled)
			servoThrottle.writeMicroseconds(vms.GetThrottle());
		else
			servoThrottle.writeMicroseconds(vms.GetHIL_Throttle()); //slow it down when using HIL
		#endif
		
		if (hilEnabled)
		{
			if (currentTime - lastHilInputSend_ >= HIL_INPUT_RATE)
			{
				lastHilInputSend_ = currentTime;
				HIL_InputStruct hilInputStruct;
				hilInputStruct.flaps = vms.GetNormalizedFlapsOutput();
				hilInputStruct.rudder = vms.GetNormalizedRudderOutput();
				hilInputStruct.ailerons = vms.GetNormalizedAileronOutput();
				hilInputStruct.elevators = vms.GetNormalizedElevatorOutput();
				hilInputStruct.throttle = vms.GetNormalizedThrottleOutput();
				uavSerial.SendData(MemoryStruct_List::HIL_INPUT, reinterpret_cast<byte*>(&hilInputStruct), sizeof(HIL_InputStruct));
			}
		}
	}
	//not connected to raspberry pi or lost connection with ground
	else
	{
		//turns off power to all servos
		digitalWrite(PIN_SERVO_RELAY, LOW);
		digitalWrite(PIN_SERVO_STATUS, LOW);
		//if we have attached servos then disable the throttle (most important part!)
		#if defined(REAL_HARDWARE)
		if (servosSet_)
			servoThrottle.writeMicroseconds(1000.0F);
		#endif
	}
	
	if (currentTime - lastHertzSend_ >= HERTZ_SEND_RATE)
	{
		ArduinoUpdateHertzStruct updateHertzStruct;
		updateHertzStruct.updateRate = (float)frameCount_ / ((float)(currentTime - lastHertzSend_) / 1000.0F);
		updateHertzStruct.availableMemory = (short)freeMemory();
		
		uavSerial.SendData(MemoryStruct_List::ARDUINO_HERTZ_RATE, reinterpret_cast<byte*>(&updateHertzStruct), sizeof(ArduinoUpdateHertzStruct));
		lastHertzSend_ = millis();
		frameCount_ = 0;
	}
	else
	{
		frameCount_++;
	}
	
	if (currentTime - lastYawPitchRollSend_ >= YAW_PITCH_ROLL_SEND_RATE)
	{
		YawPitchRollStruct yawPitchRoll;
		yawPitchRoll.yaw = ins.GetYaw();
		yawPitchRoll.pitch = ins.GetPitch();
		yawPitchRoll.roll = ins.GetRoll();
		
		uavSerial.SendData(MemoryStruct_List::YAW_PITCH_ROLL, reinterpret_cast<byte*>(&yawPitchRoll), sizeof(YawPitchRollStruct));
		lastYawPitchRollSend_ = millis();
	}
	
	if (currentTime - lastRaltSend_ >= RALT_SEND_RATE)
	{
		lastRaltSend_ = currentTime;
		RALT_Struct RALT;
		RALT.altitude = ins.GetRALT();
		uavSerial.SendData(MemoryStruct_List::RALT, reinterpret_cast<byte*>(&RALT), sizeof(RALT_Struct));
	}
	
	if (currentTime - lastGpsSend_ >= GPS_SEND_RATE)
	{
		GPS_OutputStruct gpsOutput;
		gpsOutput.longitude = ins.GetLongitude();
		gpsOutput.latitude = ins.GetLatitude();
		gpsOutput.altitude = ins.GetMSL_Altitude();
		gpsOutput.knots = ins.GetSpeed();
		
		lastGpsSend_ = currentTime;
		uavSerial.SendData(MemoryStruct_List::GPS_OUTPUT, reinterpret_cast<byte*>(&gpsOutput), sizeof(GPS_OutputStruct));
	}
	
	if (!hilEnabled)
	{
		#if defined(MPU9150_Enabled)
		MPU9150_.selectDevice(0);
		if (MPU9150_.read())
		{
			YawPitchRollStruct yawPitchRoll;
			yawPitchRoll.yaw = MPU9150_.m_fusedEulerPose[2];
			yawPitchRoll.pitch = MPU9150_.m_fusedEulerPose[1];
			yawPitchRoll.roll = MPU9150_.m_fusedEulerPose[0];
			
			AddYPR_Sample(yawPitchRoll);
			
			YawPitchRollStruct tempYPR = GetYPR_SampleAverage();
			
			ins.SetYawPitchRoll(tempYPR.yaw, tempYPR.pitch, tempYPR.roll);
		}
		#endif
	}
	
	if (!hilEnabled)
	{
		#if defined(LS2003_Enabled)
		while (Serial2.available())
		{
			if (LS2003_.encode(Serial2.read()))
			{
				GPS_OutputStruct gpsOutputStruct;
				unsigned long age;
				LS2003_.f_get_position(&gpsOutputStruct.latitude, &gpsOutputStruct.longitude, &age);
				gpsOutputStruct.altitude = LS2003_.f_altitude();
				gpsOutputStruct.knots = LS2003_.f_speed_knots();				
				ins.SetGPS_LongitudeLatitudeAltitudeSpeed(gpsOutputStruct.longitude, gpsOutputStruct.latitude, gpsOutputStruct.altitude, gpsOutputStruct.knots);				
			}
		}
		#endif
	}
	
	if (!hilEnabled)
	{
		#if defined(BMP085_Enabled)
		if (BMP085_.GetPressureTempAlt(&pressureTempAlt.temperature, &pressureTempAlt.pressure, &pressureTempAlt.altitude) && currentTime - lastPressureTempAltSend_ >= PRESSURE_TEMP_ALT_RATE)
		{
			lastPressureTempAltSend_ = currentTime;
			uavSerial.SendData(MemoryStruct_List::PRESSURE_TEMP_ALT, reinterpret_cast<byte*>(&pressureTempAlt), sizeof(PressureTempAltStruct));
		}
		#endif
	}	
	
	if (!hilEnabled)
	{
		#if defined(AUTTOPILOT_VOLTAGE_Enabled)
		voltageCurrent.voltage = (float)analogRead(PIN_VOLTAGE) / 12.99f;
		voltageCurrent.current = (float)analogRead(PIN_CURRENT) / 7.4f;
		
		if (currentTime - lastVoltageCurrentSend_ > VOLTAGE_CURRENT_SEND_RATE)
		{
			lastVoltageCurrentSend_ = currentTime;
			uavSerial.SendData(MemoryStruct_List::VOLTAGE_CURRENT, reinterpret_cast<byte*>(&voltageCurrent), sizeof(VoltageCurrentStruct));
		}
		#endif
	}
	
	ProcessAutopilot(currentTime);
}

void PacketReceived(MemoryStruct_List::DataID_ListType dataID, byte* data)
{
	if (dataID == MemoryStruct_List::TEST_LIGHT)
	{
		if (((TestLightStruct*)data)->on)
			digitalWrite(PIN_TEST_LIGHT, HIGH);
		else
			digitalWrite(PIN_TEST_LIGHT, LOW);
	}
	else if (dataID == MemoryStruct_List::AILERON_ELEVATOR_INPUT)
	{
		if (!autopilot.autopilotActive)
			vms.SetAileronInput(((AileronElevatorInputStruct*)data)->aileron);
		if (!autopilot.autopilotActive)
			vms.SetElevatorInput(((AileronElevatorInputStruct*)data)->elevator);
		lastControlSurfaceReceive_ = millis();
	}
	else if (dataID == MemoryStruct_List::RUDDER_INPUT)
	{
		if (!autopilot.autopilotActive)
			vms.SetRudderInput(((RudderInputStruct*)data)->rudder);
		lastControlSurfaceReceive_ = millis();
	}
	else if (dataID == MemoryStruct_List::THROTTLE_INPUT)
	{
		if (!autopilot.autopilotActive)
			vms.SetThrottleInput(((ThrottleInputStruct*)data)->throttle);
		lastControlSurfaceReceive_ = millis();
	}
	else if (dataID == MemoryStruct_List::FLAPS_INPUT)
	{
		//if (!autopilot.autopilotActive)
		vms.SetFlapsInput(((FlapsInputStruct*)data)->flapsDown);
		lastControlSurfaceReceive_ = millis();
	}
	else if (dataID == MemoryStruct_List::ENGINE_STATUS_INPUT)
	{
		//not using this currently
		lastControlSurfaceReceive_ = millis();
	}
	else if (dataID == MemoryStruct_List::HIL_SETTINGS)
	{
		hilEnabled = ((HIL_SettingsStruct*)data)->hilEnabled;
	}
	else if (dataID == MemoryStruct_List::HIL_OUTPUT)
	{
		if (hilEnabled)
		{
			ins.SetGPS_LongitudeLatitudeAltitudeSpeed(((HIL_OutputStruct*)data)->longitude, ((HIL_OutputStruct*)data)->latitude, ((HIL_OutputStruct*)data)->altitude, ((HIL_OutputStruct*)data)->airspeed);
			ins.SetYawPitchRoll(((HIL_OutputStruct*)data)->yaw, ((HIL_OutputStruct*)data)->pitch, ((HIL_OutputStruct*)data)->roll);	
			ins.SetRALT(((HIL_OutputStruct*)data)->RALT);
		}		
	}

	else if (dataID == MemoryStruct_List::WAYPOINT)
	{
		ArduinoWaypoint aWP;
		aWP.waypointID = ((WaypointStruct*)data)->waypointID;
		aWP.waypointType = ((WaypointStruct*)data)->waypointType;
		aWP.goToIndex = ((WaypointStruct*)data)->goToIndex;
		aWP.iterations = ((WaypointStruct*)data)->iterations;
		aWP.iterationsLeft = ((WaypointStruct*)data)->iterations;
		aWP.altitude = ((WaypointStruct*)data)->altitude;
		aWP.heading = ((WaypointStruct*)data)->heading;
		aWP.knots = ((WaypointStruct*)data)->knots;
		aWP.latitude = ((WaypointStruct*)data)->latitude;
		aWP.longitude = ((WaypointStruct*)data)->longitude;
		autopilot.waypoints[((WaypointStruct*)data)->waypointIndex] = aWP;
		autopilot.receivedWaypointCount++;
		
		if (autopilot.receivedWaypointCount == autopilot.waypointLength && autopilot.waypointLength > 0)
		{
			autopilot.routeReady = true;
			uavSerial.SendData(MemoryStruct_List::WAYPOINT_UPLOAD_COMPLETE, NULL, 0);
			autopilot.receivedWaypointCount = 0;
		}
	}
	else if (dataID == MemoryStruct_List::START_WAYPOINT_UPLOAD)
	{
		autopilot.waypointLength = (int)((StartWaypointUploadStruct*)data)->waypointCount;
		autopilot.receivedWaypointCount = 0;
		autopilot.routeReady = false;
		autopilot.autopilotActive = false;
		uavSerial.SendData(MemoryStruct_List::WAYPOINT_UPLOAD_READY, NULL, 0);
	}
	else if (dataID == MemoryStruct_List::REQUEST_AUTOPILOT_MODE)
	{
		autopilot.autopilotActive = ((AutoPilotModeStruct*)data)->autopilotEnabled && autopilot.routeReady;
	}
	else if (dataID == MemoryStruct_List::SET_CURRENT_WAYPOINT)
	{
		if (autopilot.waypointLength - 1 >= ((CurrentWaypointStruct*)data)->waypointIndex)
		autopilot.currentWaypoint = ((CurrentWaypointStruct*)data)->waypointIndex;
	}
	else if (dataID == MemoryStruct_List::WAYPOINT_DOWNLOAD_ROUTE)
	{
		if (autopilot.routeReady)
		{
			for (int i = 0; i < autopilot.waypointLength; i++)
			{
				WaypointStruct wp;
				wp.altitude = autopilot.waypoints[i].altitude;
				wp.goToIndex = autopilot.waypoints[i].goToIndex;
				wp.heading = autopilot.waypoints[i].heading;
				wp.iterations = autopilot.waypoints[i].iterations;
				wp.knots = autopilot.waypoints[i].knots;
				wp.latitude = autopilot.waypoints[i].latitude;
				wp.longitude = autopilot.waypoints[i].longitude;
				wp.waypointID = autopilot.waypoints[i].waypointID;
				wp.waypointIndex = i;
				wp.waypointType = autopilot.waypoints[i].waypointType;
				uavSerial.SendData(MemoryStruct_List::WAYPOINT, reinterpret_cast<byte*>(&wp), sizeof(WaypointStruct));
			}
			
			uavSerial.SendData(MemoryStruct_List::WAYPOINT_DOWNLOAD_COMPLETE, NULL, 0);
		}
	}
}

void ProcessAutopilot(unsigned long currentTime)
{
	autopilot.Update();

	if (currentTime - lastAutopilotModeSend_ >= AUTOPILOT_MODE_SEND_RATE)
	{
		lastAutopilotModeSend_ = currentTime;
		AutoPilotModeStruct autoPilotModeStruct;
		autoPilotModeStruct.autopilotEnabled = autopilot.autopilotActive;
		uavSerial.SendData(MemoryStruct_List::CURRENT_AUTOPILOT_MODE, reinterpret_cast<byte*>(&autoPilotModeStruct), sizeof(AutoPilotModeStruct));
	}
	
	if (autopilot.autopilotActive)
	{
		if (currentTime - lastCurrentWaypointSend_ >= CURRENT_WAYPOINT_SEND_RATE)
		{
			lastCurrentWaypointSend_ = currentTime;
			CurrentWaypointStruct currentWaypointStruct;
			currentWaypointStruct.waypointIndex = autopilot.currentWaypoint;
			uavSerial.SendData(MemoryStruct_List::GET_CURRENT_WAYPOINT, reinterpret_cast<byte*>(&currentWaypointStruct), sizeof(CurrentWaypointStruct));
		}
	}
}

void AddYPR_Sample(YawPitchRollStruct ypr)
{
	for (int i = YPR_SAMPLE_COUNT - 1; i > 0; i--)
		yprSamples_[i] = yprSamples_[i - 1];
		
	yprSamples_[0] = ypr;
}

YawPitchRollStruct GetYPR_SampleAverage()
{
	YawPitchRollStruct yawPitchRoll;
	yawPitchRoll.yaw = yprSamples_[0].yaw;
	yawPitchRoll.pitch = yprSamples_[0].pitch;
	yawPitchRoll.roll = yprSamples_[0].roll;
	
	int yawSampleCnt = 1;
	int pitchSampleCnt = 1;
	int rollSampleCnt = 1;
	
	for (int i = 1; i < YPR_SAMPLE_COUNT; i++)
	{
		if (abs(yprSamples_[i].yaw - yprSamples_[0].yaw) < PI)
		{
			yawPitchRoll.yaw += yprSamples_[i].yaw;
			yawSampleCnt++;
		}
		
		if (abs(yprSamples_[i].pitch - yprSamples_[0].pitch) < PI)
		{
			yawPitchRoll.pitch += yprSamples_[i].pitch;
			pitchSampleCnt++;
		}
		
		if (abs(yprSamples_[i].roll - yprSamples_[0].roll) < PI)
		{
			yawPitchRoll.roll += yprSamples_[i].roll;
			rollSampleCnt++;
		}
	}
	
	yawPitchRoll.yaw /= yawSampleCnt;
	yawPitchRoll.pitch /= pitchSampleCnt;
	yawPitchRoll.roll /= rollSampleCnt;
	
	yawPitchRoll.yaw *= RAD_TO_DEGREE;
	yawPitchRoll.yaw += 180; //I have the sensor in backwards
	yawPitchRoll.yaw += MAG_VAR;
	if (yawPitchRoll.yaw >= 360)
	yawPitchRoll.yaw -= 360;
	else if (yawPitchRoll.yaw < 0)
	yawPitchRoll.yaw += 360;

	yawPitchRoll.pitch *= RAD_TO_DEGREE;
	yawPitchRoll.roll *= RAD_TO_DEGREE;
	
	return yawPitchRoll;
}

float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

#if defined(MAX_SONAR_Enabled)
void calcRange()
{
	if (digitalRead(PIN_MAXSONAR) == HIGH)
	{
		interruptRangeTimerStart = micros();
	}
	else
	{
		rangePulseTime = (volatile int)micros() - interruptRangeTimerStart;
		float ralt = ((((float)rangePulseTime / 58.0f) / 2.54f) + RALT_OFFSET);
		if (!hilEnabled)
		{
			if (ralt <= 1.4f)
				ins.SetRALT(0.0f);
			else
				ins.SetRALT(ralt);
		}
	}
}
#endif
