#ifndef UAV_ARDUINO_SERIAL_H
#define UAV_ARDUINO_SERIAL_H

#include <Arduino.h>
#include <MemoryStructs.h>
//#include <avr/wdt.h>

#define RASPBERRY_TIMEOUT 2000
#define SERIAL_BUFFER_SIZE 128
#define TEMP_SERIAL_BUFFER_SIZE 64

class UAV_ArduinoSerial
{
public:
	UAV_ArduinoSerial();
	~UAV_ArduinoSerial();
	void SendData(MemoryStruct_List::DataID_ListType dataID, byte* data, int dataSize);
	bool Update();
	void SendConsole(String output);
private:	
	bool ReadSerial();
	bool ProcessSerialData();
	bool ProcessPacket(MemoryStruct_List::DataID_ListType dataID, int dataSize);
	unsigned long lastArduinoPingReceive_;
	byte serialBufferSize_;
	byte serialBuffer_[SERIAL_BUFFER_SIZE];
	byte tempSerialBuffer_[TEMP_SERIAL_BUFFER_SIZE];		
};

#endif