#include "UAV_ArduinoSerial.h"

extern void PacketReceived(MemoryStruct_List::DataID_ListType dataID, byte* data);

UAV_ArduinoSerial::UAV_ArduinoSerial()
{
	lastArduinoPingReceive_ = 0;
	serialBufferSize_ = 0;
}

UAV_ArduinoSerial::~UAV_ArduinoSerial()
{

}

void UAV_ArduinoSerial::SendData(MemoryStruct_List::DataID_ListType dataID, byte* data, int dataSize)
{
	if ((lastArduinoPingReceive_ != 0 && millis() - lastArduinoPingReceive_ < RASPBERRY_TIMEOUT) || dataID == MemoryStruct_List::CONSOLE_OUT)
	{   
		Serial.write((byte*)&dataID, DATA_ID_SIZE);
		//Serial.flush();
		delayMicroseconds(100);
		if (data > 0)
		{
			Serial.write(data, dataSize); 
			//Serial.flush();
			delayMicroseconds(100);
		}
	}
}

bool UAV_ArduinoSerial::Update()
{
	return ReadSerial();
}

bool UAV_ArduinoSerial::ReadSerial()
{  
	int dataSize = Serial.available();
	if (dataSize > 0)
	{        		
		Serial.readBytes((char*)tempSerialBuffer_, dataSize);
		if (serialBufferSize_ + dataSize < SERIAL_BUFFER_SIZE)
		{
			memcpy(serialBuffer_ + serialBufferSize_, tempSerialBuffer_, dataSize);
			serialBufferSize_ += dataSize;
			if (!ProcessSerialData())
				return false;
		}
	}
	return true;
}

bool UAV_ArduinoSerial::ProcessSerialData()
{
	bool packetComplete = true;
	while (serialBufferSize_ >= DATA_ID_SIZE && packetComplete)
	{		
		switch(serialBuffer_[0]) 
		{
		case MemoryStruct_List::TEST_LIGHT:
			packetComplete = ProcessPacket(MemoryStruct_List::TEST_LIGHT, sizeof(TestLightStruct));
			break;
		case MemoryStruct_List::ARDUINO_PING:
			packetComplete = ProcessPacket(MemoryStruct_List::ARDUINO_PING, 0);
			break;
		case MemoryStruct_List::AILERON_ELEVATOR_INPUT:
			packetComplete = ProcessPacket(MemoryStruct_List::AILERON_ELEVATOR_INPUT, sizeof(AileronElevatorInputStruct));
			break;
		case MemoryStruct_List::RUDDER_INPUT:
			packetComplete = ProcessPacket(MemoryStruct_List::RUDDER_INPUT, sizeof(RudderInputStruct));
			break;
		case MemoryStruct_List::THROTTLE_INPUT:
			packetComplete = ProcessPacket(MemoryStruct_List::THROTTLE_INPUT, sizeof(ThrottleInputStruct));
			break;
		case MemoryStruct_List::FLAPS_INPUT:
			packetComplete = ProcessPacket(MemoryStruct_List::FLAPS_INPUT, sizeof(FlapsInputStruct));
			break;
		case MemoryStruct_List::ENGINE_STATUS_INPUT:
			packetComplete = ProcessPacket(MemoryStruct_List::ENGINE_STATUS_INPUT, sizeof(EngineStatusInputStruct));
			break;
		case MemoryStruct_List::HIL_SETTINGS:
			packetComplete = ProcessPacket(MemoryStruct_List::HIL_SETTINGS, sizeof(HIL_SettingsStruct));
			break;
		case MemoryStruct_List::HIL_OUTPUT:
			packetComplete = ProcessPacket(MemoryStruct_List::HIL_OUTPUT, sizeof(HIL_OutputStruct));
			break;
		case MemoryStruct_List::WAYPOINT:
			packetComplete = ProcessPacket(MemoryStruct_List::WAYPOINT, sizeof(WaypointStruct));
			break;
		case MemoryStruct_List::START_WAYPOINT_UPLOAD:
			packetComplete = ProcessPacket(MemoryStruct_List::START_WAYPOINT_UPLOAD, sizeof(StartWaypointUploadStruct));
			break;
		case MemoryStruct_List::REQUEST_AUTOPILOT_MODE:
			packetComplete = ProcessPacket(MemoryStruct_List::REQUEST_AUTOPILOT_MODE, sizeof(AutoPilotModeStruct));
			break;
		case MemoryStruct_List::SET_CURRENT_WAYPOINT:
			packetComplete = ProcessPacket(MemoryStruct_List::SET_CURRENT_WAYPOINT, sizeof(CurrentWaypointStruct));
			break;
		case MemoryStruct_List::DOWNLOAD_ROUTE:
			packetComplete = ProcessPacket(MemoryStruct_List::DOWNLOAD_ROUTE, 0);
			break;
		default:			
			return false;
		}
	}
	return true;
}

bool UAV_ArduinoSerial::ProcessPacket(MemoryStruct_List::DataID_ListType dataID, int dataSize)
{
	if (serialBufferSize_ >= DATA_ID_SIZE + dataSize)
	{
		if (dataID == MemoryStruct_List::ARDUINO_PING)
		{
			lastArduinoPingReceive_ = millis();
		}
		else
		{
			::PacketReceived(dataID, serialBuffer_ + DATA_ID_SIZE);			
		}
		//we ate some bytes
		serialBufferSize_ -= (DATA_ID_SIZE + dataSize);
		//shift data down
		memcpy(serialBuffer_, serialBuffer_ + (DATA_ID_SIZE + dataSize), serialBufferSize_);
		return true;
	}
	else
	{
		return false;
	}
}

void UAV_ArduinoSerial::SendConsole(String output)
{	
	ConsoleStruct consoleOut;
	output.toCharArray(consoleOut.console, CONSOLE_SIZE); 
	SendData(MemoryStruct_List::CONSOLE_OUT, reinterpret_cast<byte*>(&consoleOut), sizeof(ConsoleStruct));
}