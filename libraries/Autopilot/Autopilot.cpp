#include "Autopilot.h"

Autopilot::Autopilot()
{
	routeReady = false;
	autopilotActive = false;
	waypointLength = 0;
	currentWaypoint = 0;
	receivedWaypointCount = -1;
	lastDebugPrint = 0;
	lastUpdate = 0;
	integralPitchError = 0.0f;
	integralRollError = 0.0f;
}

Autopilot::~Autopilot()
{

}

void Autopilot::SetINS(UAV_INS* ins)
{
	Autopilot::ins = ins;
}

void Autopilot::SetVMS(UAV_VMS* vms)
{
	Autopilot::vms = vms;
}

void Autopilot::SetSerial(UAV_ArduinoSerial* uavSerial)
{
	Autopilot::uavSerial = uavSerial;
}

void Autopilot::Update()
{
	unsigned long newUpdate = micros();
	float updateTime = ((newUpdate - lastUpdate) / 1000000.0f);
	lastUpdate = newUpdate;
	
	if (routeReady && autopilotActive)
	{
		vms->SetThrottleInput(1.0f);
		
		if (waypoints[currentWaypoint].waypointType == WaypointTypeEnum::WAYPOINT)
		{		
			//+roll = right, -roll = left
			//+yaw = right, -yaw = left
			//+ailerons = right, -ailerons = left
			//+elevators = up, -elevators = down
			
			float ownshipX, ownshipY, ownshipZ;
			float waypointX, waypointY, waypointZ;
			CoordinateSystemHelper::ConvertSphericalToCartesian(ins->GetLongitude(), ins->GetLatitude(), ins->GetMSL_Altitude(), ownshipX, ownshipY, ownshipZ);
			CoordinateSystemHelper::ConvertSphericalToCartesian(waypoints[currentWaypoint].longitude, waypoints[currentWaypoint].latitude, waypoints[currentWaypoint].altitude, waypointX, waypointY, waypointZ);
			float slantRange = CoordinateSystemHelper::GetSlantRange(ownshipX, ownshipY, ownshipZ, waypointX, waypointY, waypointZ) * KM_TO_FEET;
			//Great circle distance to waypoint
			float distance = CoordinateSystemHelper::GetGreatCircleDistance(ins->GetLongitude(), ins->GetLatitude(), ins->GetMSL_Altitude(), waypoints[currentWaypoint].longitude, waypoints[currentWaypoint].latitude) * KM_TO_FEET;
			//Elevation angle to waypoint
			float wpElevationAngle = CoordinateSystemHelper::GetElevationAngle(ins->GetMSL_Altitude(), waypoints[currentWaypoint].altitude, slantRange);
			float altitudeDifference = waypoints[currentWaypoint].altitude - ins->GetMSL_Altitude();
			
			//-------Proportional Yaw/Roll-------//
			float wpHeading = CoordinateSystemHelper::GetHeadingToCoordinte(ins->GetLongitude(), ins->GetLatitude(), waypoints[currentWaypoint].longitude, waypoints[currentWaypoint].latitude);
			float convertedHeading = UAV_INS::NormalizeDegrees(ins->GetYaw());
			float headingDiff = UAV_INS::NormalizeDegrees(wpHeading - convertedHeading);						
			float targetedRoll = pow(fabs(headingDiff), 0.8f);								
			targetedRoll = headingDiff > 0.0f ? targetedRoll: -targetedRoll;		
			if (targetedRoll > MAX_ROLL)
				targetedRoll = MAX_ROLL;
			else if (targetedRoll < -MAX_ROLL)
				targetedRoll = -MAX_ROLL;
			float rollError = targetedRoll - ins->GetRoll();
			float proportionalRollConstant = 1.2f;
			float proportionalAilerons = proportionalRollConstant * (rollError / 180.0f);	
			
			//-------Integral Yaw/Roll-------//	
			float adjustRollConsant = 1.0f;
			float integralRollConstant = 0.003f;
			if (ins->GetRoll() > MAX_ROLL)			
				integralRollError -= adjustRollConsant * updateTime;			
			else if (ins->GetRoll() < -MAX_ROLL)			
				integralRollError += adjustRollConsant * updateTime;			
			else							
				integralRollError += rollError * updateTime;
			
			//Don't let integralRollError account for more than maxIntegralRollDeflection of aileron deflection
			float maxIntegralRollDeflection = 0.3f;
			if (integralRollError > maxIntegralRollDeflection / integralRollConstant)
				integralRollError = integralRollConstant * maxIntegralRollDeflection;
			else if (integralPitchError < -maxIntegralRollDeflection / integralRollConstant)
				integralRollError = integralRollConstant * -maxIntegralRollDeflection;
				
			float integralAilerons = integralRollConstant * integralRollError;			
						
			//-------Proportional Pitch-------//
			float targetedPitch = pow(fabs(altitudeDifference), 0.4f);
			targetedPitch = altitudeDifference > 0.0f ? targetedPitch: -targetedPitch;
			if (targetedPitch > MAX_PITCH)
				targetedPitch = MAX_PITCH;
			else if (targetedPitch < -MAX_PITCH)
				targetedPitch = -MAX_PITCH;
			float pitchError = targetedPitch - ins->GetPitch();
			float proportionalPitchConstant = 10.0f;														
			float proportionalElevators = proportionalPitchConstant * (pitchError / 180.0f);
			
			//-------Integral Pitch-------//	
			float adjustPitchConsant = 2.0f;
			float integralPitchConstant = 0.01f;
			if (ins->GetPitch() > MAX_PITCH)			
				integralPitchError -= adjustPitchConsant * updateTime;						
			else if (ins->GetPitch() < -MAX_PITCH)			
				integralPitchError += adjustPitchConsant * updateTime;				
			else							
				integralPitchError += pitchError * updateTime;
				
			//Don't let integralPitchError account for more than 50% of elevator deflection
			if (integralPitchError > 0.5f / integralPitchConstant)
				integralPitchError = integralPitchConstant * 0.5f;
			else if (integralPitchError < -0.5f / integralPitchConstant)
				integralPitchError = integralPitchConstant * -0.5f;
				
			float integralElevators = integralPitchConstant * integralPitchError;											
			
			vms->SetAileronInput(proportionalAilerons + integralAilerons);
			vms->SetElevatorInput(proportionalElevators + integralElevators);
			
			if (slantRange <= WP_HIT_RANGE)
			{
				currentWaypoint++;
				if (currentWaypoint + 1 > waypointLength)
					currentWaypoint = 0;			
			}
			
			//if (millis() - lastDebugPrint > 100)
			//{
				//lastDebugPrint = millis();
				//char tmp[10];
				//String headStr = "sr: ";
				//headStr += floatToString(tmp, slantRange, 3, 7); //test this
				//uavSerial->SendConsole(headStr);
			//}						
		}
	}
	else
	{
		integralPitchError = 0.0f;		
		integralRollError = 0.0f;
	}
	

}

char* Autopilot::floatToString(char * outstr, double val, byte precision, byte widthp)
{
	char temp[16]; //increase this if you need more digits than 15
	byte i;

	temp[0]='\0';
	outstr[0]='\0';

	if(val < 0.0){
		strcpy(outstr,"-\0");  //print "-" sign
		val *= -1;
	}

	if( precision == 0) {
		strcat(outstr, ltoa(round(val),temp,10));  //prints the int part
	}
	else {
		unsigned long frac, mult = 1;
		byte padding = precision-1;
		
		while (precision--)
		mult *= 10;

		val += 0.5/(float)mult;      // compute rounding factor
		
		strcat(outstr, ltoa(floor(val),temp,10));  //prints the integer part without rounding
		strcat(outstr, ".\0"); // print the decimal point

		frac = (val - floor(val)) * mult;

		unsigned long frac1 = frac;

		while(frac1 /= 10)
		padding--;

		while(padding--)
		strcat(outstr,"0\0");    // print padding zeros

		strcat(outstr,ltoa(frac,temp,10));  // print fraction part
	}

	// generate width space padding
	if ((widthp != 0)&&(widthp >= strlen(outstr))){
		byte J=0;
		J = widthp - strlen(outstr);

		for (i=0; i< J; i++) {
			temp[i] = ' ';
		}

		temp[i++] = '\0';
		strcat(temp,outstr);
		strcpy(outstr,temp);
	}

	return outstr;
}