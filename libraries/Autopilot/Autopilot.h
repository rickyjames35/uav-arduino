#ifndef AUTOPILOT_H
#define AUTOPILOT_H

#include <Arduino.h>
#include "CoordinateSystemHelper.h"
#include "MemoryStructs.h"
#include "UAV_VMS.h"
#include "UAV_ArduinoSerial.h"
#include "UAV_INS.h"

#define MAX_PITCH 15.0f
#define MAX_ROLL 35.0f

#define WP_HIT_RANGE 500.0f

struct ArduinoWaypoint
{
    unsigned char waypointID;
    unsigned char waypointType;
    unsigned char goToIndex;
    unsigned char iterations;
	unsigned char iterationsLeft;
    float altitude;
    float heading;
    float latitude;
    float longitude;       
	float knots;
};

class Autopilot
{
public:
	Autopilot();
	~Autopilot();
	void Update();
	void SetVMS(UAV_VMS* vms);
	void SetSerial(UAV_ArduinoSerial* uavSerial);
	void SetINS(UAV_INS* ins);

	ArduinoWaypoint waypoints[50];
	int currentWaypoint;
	int waypointLength;
	int receivedWaypointCount;
	boolean autopilotActive;
	boolean routeReady;

private:
	char* floatToString(char * outstr, double val, byte precision, byte widthp);

	UAV_VMS* vms;
	UAV_ArduinoSerial* uavSerial;
	UAV_INS* ins;
	unsigned long lastDebugPrint;
	unsigned long lastUpdate;
	
	float integralPitchError;
	float integralRollError;
};

#endif