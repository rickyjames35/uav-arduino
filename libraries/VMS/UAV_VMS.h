#ifndef UAV_VMS_H
#define UAV_VMS_H

class UAV_VMS
{
public:
	UAV_VMS();
	~UAV_VMS();

	void SetRudderInput(float rudderInput);
	void SetAileronInput(float aileronInput);
	void SetElevatorInput(float elevatorInput);
	void SetFlapsInput(bool flapsDown);
	void SetThrottleInput(float throttleInput);
		
	int GetRudderOutput();
	int GetAileronOutput();
	int GetElevatorOutput();
	int GetFlapsOutput();
	int GetThrottle();
	int GetHIL_Throttle();	

	float GetNormalizedRudderOutput();
	float GetNormalizedAileronOutput();
	float GetNormalizedElevatorOutput();
	float GetNormalizedFlapsOutput();
	float GetNormalizedThrottleOutput();
	float GetHIL_NormalizedThrottleOutput();

private:
	float commandedRudderPosition_;
	float commandedAileronPosition_;
	float commandedElevatorPosition_;
	float commandedFlapsPosition_;
	float commandedThrottle_;

	int toDegrees(float input);
	int toRudderDegrees(float input);
	int toElevatorDegrees(float input);
	int toAileronDegrees(float input);
	int toFlapsDegrees(float input);
	int toThrottleDegrees(float input);
	int toHIL_ThrottleDegrees(float input);
};

#endif