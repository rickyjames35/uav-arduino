#include "UAV_VMS.h"

UAV_VMS::UAV_VMS()
{
	commandedRudderPosition_ = 0.0F;
	commandedAileronPosition_ = 0.0F;
	commandedElevatorPosition_ = 0.0F;
	commandedFlapsPosition_ = 0.0F;
	commandedThrottle_ = 0.0F;
}

UAV_VMS::~UAV_VMS()
{

}

void UAV_VMS::SetRudderInput(float rudderInput)
{	
	commandedRudderPosition_ = rudderInput > 1.0f ? 1.0f : rudderInput < -1.0f ? -1.0f : rudderInput;
}

void UAV_VMS::SetAileronInput(float aileronInput)
{
	commandedAileronPosition_ = aileronInput > 1.0f ? 1.0f : aileronInput < -1.0f ? -1.0f : aileronInput;
}

void UAV_VMS::SetElevatorInput(float elevatorInput)
{
	commandedElevatorPosition_ = elevatorInput > 1.0f ? 1.0f : elevatorInput < -1.0f ? -1.0f : elevatorInput;
}

void UAV_VMS::SetFlapsInput(bool flapsDown)
{
	commandedFlapsPosition_ = flapsDown ? -0.8 : 0;
}

void UAV_VMS::SetThrottleInput(float throttleInput)
{
	commandedThrottle_ = throttleInput;
}

int UAV_VMS::GetRudderOutput()
{
	return toRudderDegrees(commandedRudderPosition_);
}

int UAV_VMS::GetAileronOutput()
{
	return toAileronDegrees(commandedAileronPosition_);
}

int UAV_VMS::GetElevatorOutput()
{
	return toElevatorDegrees(commandedElevatorPosition_);
}

int UAV_VMS::GetFlapsOutput()
{
	return toFlapsDegrees(commandedFlapsPosition_);
}

int UAV_VMS::GetThrottle()
{
	return toThrottleDegrees(commandedThrottle_);
}

int UAV_VMS::GetHIL_Throttle()
{
	return toHIL_ThrottleDegrees(commandedThrottle_);
}

float UAV_VMS::GetNormalizedRudderOutput()
{
	return commandedRudderPosition_;
}

float UAV_VMS::GetNormalizedAileronOutput()
{
	return commandedAileronPosition_;
}

float UAV_VMS::GetNormalizedElevatorOutput()
{
	return commandedElevatorPosition_;
}

float UAV_VMS::GetNormalizedFlapsOutput()
{
	return commandedFlapsPosition_;
}

float UAV_VMS::GetNormalizedThrottleOutput()
{
	return commandedThrottle_;
}

//min = 45, max = 135
int UAV_VMS::toElevatorDegrees(float input)
{
	return ((input * 0.50F) + 1.0F) * 90.0F;	
}

//min = 67.5 max = 112.5
int UAV_VMS::toRudderDegrees(float input)
{
	return ((input * 0.25F) + 1.0F) * 90.0F;
}

int UAV_VMS::toAileronDegrees(float input)
{
	return 180.0F - (((input * 0.4F) + 1.0F) * 90.0F);
}

int UAV_VMS::toFlapsDegrees(float input)
{
	return 180.0F - (((input * 0.4F) + 1.0F) * 90.0F); 
}

int UAV_VMS::toDegrees(float input)
{
	return (input + 1.0F) * 90.0F;
}

int UAV_VMS::toThrottleDegrees(float input)
{
	//motor doesn't kick on until it's at about 10% so I scaled it up by 10%
	if (input > 0.0f)	
		return (input * 900.0F) + 1100.0F;
	else
		return 1000.0f;
}

int UAV_VMS::toHIL_ThrottleDegrees(float input)
{
	//restrict the motor for going over 13% while running HIL
	//if (input > 0.0f)	
	//	return (input * 30.0F) + 1100.0F;
	//else
		return 1000.0f;
}