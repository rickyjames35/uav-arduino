#include "UAV_INS.h"

UAV_INS::UAV_INS()
{
	yaw = pitch = roll = 0.0f;
	longitude = latitude = gpsAltitude = speed = 0.0f;
	yawPitchRollFirstSet = true;
	yawPitchRollLastSet = 0;
	RALT = 0.0f;
	WOW = false;
}

void UAV_INS::SetGPS_LongitudeLatitudeAltitudeSpeed(float longitude, float latitude, float altitude, float speed)
{
	UAV_INS::longitude = longitude;
	UAV_INS::latitude = latitude;
	UAV_INS::gpsAltitude = altitude;
	UAV_INS::speed = speed;
}

void UAV_INS::SetYawPitchRoll(float yaw, float pitch, float roll)
{
	if (yawPitchRollFirstSet)
	{		
		yawPitchRollFirstSet = false;
		UAV_INS::yaw = UAV_INS::NormalizeDegrees(yaw);
		UAV_INS::pitch = UAV_INS::NormalizeDegrees(pitch);
		UAV_INS::roll = UAV_INS::NormalizeDegrees(-roll);
	}
	else
	{
		float timePast = (float)((micros() - yawPitchRollLastSet) / 1000000.0f);				

		yawRate = timePast == 0.0f ? 0.0f : UAV_INS::NormalizeDegrees(UAV_INS::NormalizeDegrees(yaw) - UAV_INS::yaw) / timePast;			
		pitchRate = timePast == 0.0f ? 0.0f : UAV_INS::NormalizeDegrees(UAV_INS::NormalizeDegrees(pitch) - UAV_INS::pitch) / timePast;
		rollRate = timePast == 0.0f ? 0.0f : UAV_INS::NormalizeDegrees(UAV_INS::NormalizeDegrees(-roll) - UAV_INS::roll) / timePast;
		
		UAV_INS::yaw = UAV_INS::NormalizeDegrees(yaw);
		UAV_INS::pitch = UAV_INS::NormalizeDegrees(pitch);
		UAV_INS::roll = UAV_INS::NormalizeDegrees(-roll);
	}
	
	yawPitchRollLastSet = micros();
}

void UAV_INS::Update()
{
	
}

float UAV_INS::GetYaw()
{
	float timePast = (float)(micros() - yawPitchRollLastSet) / 1000000.0f;	
	return yaw + (GetYawRate() * timePast);
}

float UAV_INS::GetPitch()
{
	float timePast = (float)(micros() - yawPitchRollLastSet) / 1000000.0f;
	return pitch + (pitchRate * timePast);
}

float UAV_INS::GetRoll()
{
	float timePast = (float)(micros() - yawPitchRollLastSet) / 1000000.0f;
	return roll + (rollRate * timePast);
}

float UAV_INS::GetLongitude()
{
	return longitude;
}

float UAV_INS::GetLatitude()
{
	return latitude;
}

float UAV_INS::GetMSL_Altitude()
{
	return gpsAltitude;
}

float UAV_INS::GetSpeed()
{
	return speed;
}

float UAV_INS::GetYawRate()
{
	return yawRate;
}

float UAV_INS::GetPitchRate()
{
	return pitchRate;
}

float UAV_INS::GetRollRate()
{
	return rollRate;
}

float UAV_INS::NormalizeDegrees(float degrees)
{
	if (degrees > 180)
		degrees = -(360 - degrees);
	else if (degrees < -180)
		degrees = 360 + degrees;

	return degrees;
}

float UAV_INS::GetRALT()
{
	return RALT;
}

boolean UAV_INS::GetWOW()
{
	return RALT < 6.0f;
	
}

void UAV_INS::SetRALT(float ralt)
{
	RALT = ralt;
}