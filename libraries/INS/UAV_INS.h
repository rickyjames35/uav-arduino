#ifndef UAV_INS_H
#define UAV_INS_H

#include <Arduino.h>

class UAV_INS
{
public:
	UAV_INS();
	~UAV_INS() { };

	void Update();
	void SetYawPitchRoll(float yaw, float pitch, float roll);
	void SetGPS_LongitudeLatitudeAltitudeSpeed(float longitude, float latitude, float altitude, float speed);
	void SetRALT(float ralt);

	float GetYaw();
	float GetPitch();
	float GetRoll();
	
	float GetYawRate();
	float GetPitchRate();
	float GetRollRate();

	float GetLongitude();
	float GetLatitude();
	float GetMSL_Altitude();
	float GetSpeed();
	
	float GetRALT();
	boolean GetWOW();

	static float NormalizeDegrees(float degrees);
private:

	bool yawPitchRollFirstSet;
	unsigned long yawPitchRollLastSet;
	float yaw, pitch, roll;
	float yawRate, pitchRate, rollRate;	
	
	float longitude, latitude, gpsAltitude, speed;
	
	float RALT;
	boolean WOW;
};

#endif