#include "CoordinateSystemHelper.h"

void CoordinateSystemHelper::ConvertSphericalToCartesian(float longitude, float latitude, float altitude, float& x, float& y, float& z)
{
	float latRad = latitude * DEGREE_TO_RAD;
	float lonRad = longitude * DEGREE_TO_RAD;	
	float tempAlt = EARTH_RADIUS_KM + ((FEET_TO_KM * altitude) / 2.0f);
	x = tempAlt * cos(latRad) * cos(lonRad);
	y = tempAlt * cos(latRad) * sin(lonRad);
	z = tempAlt * sin(latRad);			
}

float CoordinateSystemHelper::GetHeadingToCoordinte(float aLongitude, float aLatitude, float bLongitude, float bLatitude)
{
	aLongitude *= DEGREE_TO_RAD;
	aLatitude *= DEGREE_TO_RAD;
	bLongitude *= DEGREE_TO_RAD;
	bLatitude *= DEGREE_TO_RAD;

	float deltaLon = bLongitude - aLongitude;
	float y = sin(deltaLon) * cos(bLatitude);
	float x = cos(aLatitude) * sin(bLatitude) - sin(aLatitude) * cos(bLatitude) * cos(deltaLon);
	return atan2(y, x) * RAD_TO_DEGREE;
}

float CoordinateSystemHelper::GetElevationAngle(float aAltitude, float bAltitude, float distance)
{
	if (distance == 0.0f)
	{
		return 0.0f;
	}
	else		
	{
		float a = (bAltitude - aAltitude) / distance;
		return asin(a > 1.0f ? 1.0f : a < -1.0f ? -1.0f : a) * RAD_TO_DEGREE;	
	}		
}

float CoordinateSystemHelper::GetSlantRange(float aX, float aY, float aZ, float bX, float bY, float bZ)
{
	float deltaX = bX - aX;
	float deltaY = bY - aY;
	float deltaZ = bZ - aZ;
	return sqrt((deltaX * deltaX) + (deltaY * deltaY) + (deltaZ * deltaZ));
}

float CoordinateSystemHelper::GetGreatCircleDistance(float aLongitude, float aLatitude, float aAltitude, float bLongitude, float bLatitude)
{
	aLongitude *= DEGREE_TO_RAD;
	aLatitude *= DEGREE_TO_RAD;
	bLongitude *= DEGREE_TO_RAD;
	bLatitude *= DEGREE_TO_RAD;

	float deltaLat = bLatitude - aLatitude;
	float deltaLon = bLongitude - aLongitude;
	float sinSqLat = sin(deltaLat / 2.0f);
	float sinSqLon = sin(deltaLon / 2.0f);
	sinSqLat *= sinSqLat;
	sinSqLon *= sinSqLon;
	float a = sinSqLat + cos(aLatitude) * cos(bLatitude) * sinSqLon;	
	return 2.0 * EARTH_RADIUS_KM * asin(sqrt(a));
}