#ifndef COORDINATE_SYSTEM_HELPER_H
#define COORDINATE_SYSTEM_HELPER_H

#include <Arduino.h>

#define	RAD_TO_DEGREE (180.0f / (float)M_PI)
#define DEGREE_TO_RAD ((float)M_PI / 180.0f)
#define EARTH_RADIUS_KM 6371.0f
#define FEET_TO_KM 0.0003048f
#define KM_TO_FEET 3280.84f

class CoordinateSystemHelper
{
public:
	static void ConvertSphericalToCartesian(float longitude, float latitude, float altitude, float& x, float& y, float&z );
	static float GetHeadingToCoordinte(float aLongitude, float aLatitude, float bLongitude, float bLatitude);
	static float GetElevationAngle(float aAltitude, float bAltitude, float distance);
	static float GetSlantRange(float aX, float aY, float aZ, float bX, float bY, float bZ);
	static float GetGreatCircleDistance(float aLongitude, float aLatitude, float aAltitude, float bLongitude, float bLatitude);
};

#endif