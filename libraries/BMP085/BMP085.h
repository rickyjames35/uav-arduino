#ifndef BMP085_H
#define BMP085_H

#include <Wire.h>
#include "Arduino.h"

#define BMP085_ADDRESS 0x77  // I2C address of BMP085
#define OSS 0 // Oversampling Setting

class BMP085
{
public:
	BMP085();
	~BMP085();
	void bmp085Calibration();	
	bool GetPressureTempAlt(short* temp, long* pressure, float* alt);

private:
	int bmp085ReadInt(unsigned char address);	
	short bmp085GetTemperature(unsigned int ut);
	long bmp085GetPressure(unsigned long up);
	bool GetTemperature(short* temperature);
	bool GetPressure(long* pressure);
	float GetAltitude(long pressure);

	short tempTemp;
	bool tempTurn;
	bool tempSet;
	bool pressureSet;
	unsigned long tempWait;
	unsigned long pressureWait;

	// Calibration values
	int ac1;
	int ac2; 
	int ac3; 
	unsigned int ac4;
	unsigned int ac5;
	unsigned int ac6;
	int b1; 
	int b2;
	int mb;
	int mc;
	int md;

	// b5 is calculated in bmp085GetTemperature(...), this variable is also used in bmp085GetPressure(...)
	// so ...Temperature(...) must be called before ...Pressure(...).
	long b5; 

	static const float p0 = 101325;     // Pressure at sea level (Pa)
};

#endif