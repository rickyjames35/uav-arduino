#include "BMP085.h"

BMP085::BMP085()
{
	tempSet = false;
	pressureSet = false;
	tempTurn = true;
}

BMP085::~BMP085()
{

}

// Read 2 bytes from the BMP085
// First byte will be from 'address'
// Second byte will be from 'address'+1
int BMP085::bmp085ReadInt(unsigned char address)
{
	unsigned char msb, lsb;

	Wire.beginTransmission(BMP085_ADDRESS);
	Wire.write(address);
	Wire.endTransmission();

	Wire.requestFrom(BMP085_ADDRESS, 2);
	while(Wire.available()<2)
		;
	msb = Wire.read();
	lsb = Wire.read();

	return (int) msb<<8 | lsb;
}

// Stores all of the bmp085's calibration values into global variables
// Calibration values are required to calculate temp and pressure
// This function should be called at the beginning of the program
void BMP085::bmp085Calibration()
{
	ac1 = bmp085ReadInt(0xAA);
	ac2 = bmp085ReadInt(0xAC);
	ac3 = bmp085ReadInt(0xAE);
	ac4 = bmp085ReadInt(0xB0);
	ac5 = bmp085ReadInt(0xB2);
	ac6 = bmp085ReadInt(0xB4);
	b1 = bmp085ReadInt(0xB6);
	b2 = bmp085ReadInt(0xB8);
	mb = bmp085ReadInt(0xBA);
	mc = bmp085ReadInt(0xBC);
	md = bmp085ReadInt(0xBE);
}

// Calculate temperature given ut.
// Value returned will be in units of 0.1 deg C
short BMP085::bmp085GetTemperature(unsigned int ut)
{
	long x1, x2;

	x1 = (((long)ut - (long)ac6)*(long)ac5) >> 15;
	x2 = ((long)mc << 11)/(x1 + md);
	b5 = x1 + x2;

	return ((b5 + 8)>>4);  
}

// Calculate pressure given up
// calibration values must be known
// b5 is also required so bmp085GetTemperature(...) must be called first.
// Value returned will be pressure in units of Pa.
long BMP085::bmp085GetPressure(unsigned long up)
{
	long x1, x2, x3, b3, b6, p;
	unsigned long b4, b7;

	b6 = b5 - 4000;
	// Calculate B3
	x1 = (b2 * (b6 * b6)>>12)>>11;
	x2 = (ac2 * b6)>>11;
	x3 = x1 + x2;
	b3 = (((((long)ac1)*4 + x3)<<OSS) + 2)>>2;

	// Calculate B4
	x1 = (ac3 * b6)>>13;
	x2 = (b1 * ((b6 * b6)>>12))>>16;
	x3 = ((x1 + x2) + 2)>>2;
	b4 = (ac4 * (unsigned long)(x3 + 32768))>>15;

	b7 = ((unsigned long)(up - b3) * (50000>>OSS));
	if (b7 < 0x80000000)
		p = (b7<<1)/b4;
	else
		p = (b7/b4)<<1;

	x1 = (p>>8) * (p>>8);
	x1 = (x1 * 3038)>>16;
	x2 = (-7357 * p)>>16;
	p += (x1 + x2 + 3791)>>4;

	return p;
}

//devide by 10, in Celsius
bool BMP085::GetTemperature(short* temperature)
{
	if (!tempSet)
	{
		tempSet = true;
		unsigned int ut;

		// Write 0x2E into Register 0xF4
		// This requests a temperature reading
		Wire.beginTransmission(BMP085_ADDRESS);
		Wire.write(0xF4);
		Wire.write(0x2E);
		Wire.endTransmission();
		tempWait = millis();
	}
	
	if (millis() - tempWait >= 5)
	{
		tempSet = false;
		*temperature = bmp085GetTemperature((unsigned int)bmp085ReadInt(0xF6));
		return true;
	}

	return false;
}

//Pa
bool BMP085::GetPressure(long* pressure)
{
	if (!pressureSet)
	{
		pressureSet = true;		
		// Write 0x34+(OSS<<6) into register 0xF4
		// Request a pressure reading w/ oversampling setting
		Wire.beginTransmission(BMP085_ADDRESS);
		Wire.write(0xF4);
		Wire.write(0x34 + (OSS<<6));
		Wire.endTransmission();
	}

	if (millis() - pressureWait >= (2 + (3<<OSS)))
	{
		pressureSet = false;
		unsigned char msb, lsb, xlsb;
		// Read register 0xF6 (MSB), 0xF7 (LSB), and 0xF8 (XLSB)
		Wire.beginTransmission(BMP085_ADDRESS);
		Wire.write(0xF6);
		Wire.endTransmission();
		Wire.requestFrom(BMP085_ADDRESS, 3);

		// Wait for data to become available
		while(Wire.available() < 3)
			;
		msb = Wire.read();
		lsb = Wire.read();
		xlsb = Wire.read();

		*pressure = bmp085GetPressure((((unsigned long) msb << 16) | ((unsigned long) lsb << 8) | (unsigned long) xlsb) >> (8-OSS));
		return true;
	}

	return false;
}

//meters MSL
float BMP085::GetAltitude(long pressure)
{
	return (float)44330 * (1 - pow(((float) pressure/p0), 0.190295));
}

bool BMP085::GetPressureTempAlt(short* temp, long* pressure, float* alt)
{
	if (tempTurn)
	{
		tempTurn = !GetTemperature(&tempTemp);
	}
	
	if (!tempTurn)
	{		
		*temp = tempTemp;
		tempTurn = GetPressure(pressure);
		*alt = GetAltitude(*pressure);
		return true;
	}

	return false;
}