#ifndef MEMORY_STRUCTS_H
#define MEMORY_STRUCTS_H

#define DATA_ID_SIZE 1
#define CONSOLE_SIZE 24

#pragma pack (push, 1)

namespace WaypointTypeEnum
{
	enum Type { TAKEOFF, LAND, WAYPOINT, GOTO };
}

namespace MemoryStruct_List
{
	enum DataID_ListType
	{
        START_STOP_VIDEO,
        TEST_LIGHT,
        ARDUINO_HERTZ_RATE,
        RASPBERRY_PI_HERTZ_RATE,
        YAW_PITCH_ROLL,
        ARDUINO_PING,
        AILERON_ELEVATOR_INPUT,
        RUDDER_INPUT,
        THROTTLE_INPUT,
        FLAPS_INPUT,
        ENGINE_STATUS_INPUT,
        CONNECTION_INFO,
        GPS_OUTPUT,
        HIL_SETTINGS,
        HIL_OUTPUT,
        HIL_INPUT,
        PRESSURE_TEMP_ALT,
        RALT,
        VOLTAGE_CURRENT,
        WAYPOINT,
        START_WAYPOINT_UPLOAD,
        WAYPOINT_UPLOAD_READY,
        WAYPOINT_UPLOAD_COMPLETE,
        REQUEST_AUTOPILOT_MODE,
        CURRENT_AUTOPILOT_MODE,
        SET_CURRENT_WAYPOINT,
        GET_CURRENT_WAYPOINT,
        GET_GOTO_ITERATIONS,
		CONSOLE_OUT,
		WAYPOINT_DOWNLOAD_ROUTE,
		WAYPOINT_DOWNLOAD_COMPLETE
	};
}

struct ConsoleStruct
{
	char console[CONSOLE_SIZE];
};

struct GotoIterationsStruct
{
    unsigned char waypointIndex;
    unsigned char iterationsLeft;
};

struct CurrentWaypointStruct
{
    unsigned char waypointIndex;
};

struct AutoPilotModeStruct
{
    bool autopilotEnabled;
};

struct StartWaypointUploadStruct
{
    unsigned char waypointCount;
};

struct WaypointStruct
{
    float altitude;
    float heading;
	float knots;
    float latitude;
    float longitude;    
	unsigned char waypointIndex;
    unsigned char waypointID;
    unsigned char waypointType;
    unsigned char goToIndex;
    unsigned char iterations;
};

struct VoltageCurrentStruct
{
	float voltage;
	float current;
};

struct RALT_Struct
{
	float altitude; //inches
};

struct PressureTempAltStruct
{
	long pressure; //pa
	float altitude; //meters MSL
	short temperature; //celsius x10	
};

struct HIL_SettingsStruct
{
    bool hilEnabled;
};

struct HIL_OutputStruct
{
    float latitude;
    float longitude;        
	float altitude; //MSL feet
	float RALT;
	float airspeed; //kt	
	float yaw;
    float pitch;
	float roll;
};

struct HIL_InputStruct
{
    float flaps;
    float rudder;
    float ailerons;
    float elevators;
    float throttle;
};

struct GPS_OutputStruct
{
	float longitude;
	float latitude;	
	float altitude;
	float knots;
};

struct ConnectionInfoStruct
{
    unsigned char firstOctet;
    unsigned char secondOctet;
    unsigned char thirdOctet;
    unsigned char fourthOctet;
};

struct RaspberryPiUpdateHertzStruct
{
	float updateRate;
	float signalStrength;
	int txBps;
	int rxBps;
};

struct ArduinoUpdateHertzStruct
{
	float updateRate;
	short availableMemory;
};

struct YawPitchRollStruct
{
	float yaw;
	float pitch;
	float roll;
};

struct StartStopVideoStruct
{
	bool start;
};

struct TestLightStruct
{
	bool on;
};

struct AileronElevatorInputStruct
{
	float aileron;
	float elevator;
};

struct RudderInputStruct
{
	float rudder;
};

struct ThrottleInputStruct
{
	float throttle;
};

struct FlapsInputStruct
{
	bool flapsDown;
};

struct EngineStatusInputStruct
{
	bool engineOn;
};

#pragma pack (pop)

#endif